package domjutgeproblemAJRB;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class case1 {

	@Test
	void testosBasics() {

		ArrayList<String> list = new ArrayList<String>(Arrays.asList("res"));

		ArrayList<String> list1 = new ArrayList<String>(
				Arrays.asList("PARACEtamol", "Frenadol", "Nolotil", "Marihuana", "Insulina"));

		ArrayList<String> list2 = new ArrayList<String>(Arrays.asList("Ibuprofeno", "IBUPROFENO", "IBUproFENO", "kekw",
				"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."));

		ArrayList<String> list3 = new ArrayList<String>(
				Arrays.asList("MasCARETA", "mascareta", "ibuprofeno", "novavax", "inovio", "inovio"));

		ArrayList<String> list4 = new ArrayList<String>(Arrays.asList("mascareta", "ibuprofeno", "novavax"));

		assertEquals("Good", problemsolAJRB.infectonator(list));
		assertEquals("Good", problemsolAJRB.infectonator(list1));
		assertEquals("Infectat 1", problemsolAJRB.infectonator(list2));
		assertEquals("Infectat 4", problemsolAJRB.infectonator(list3));
		assertEquals("Infectat 3", problemsolAJRB.infectonator(list4));

	}

}
